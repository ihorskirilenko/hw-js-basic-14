'use strict'

/**
 * Завдання
 * Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому Javascript без використання
 * бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * Взяти будь-яке готове домашнє завдання з HTML/CSS.
 * Додати на макеті кнопку "Змінити тему".
 * При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання -
 * повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
 * Вибрана тема повинна зберігатися після перезавантаження сторінки *
 * */

localStorage.getItem('theme') ? false : localStorage.setItem('theme', 'defaultTheme');

window.addEventListener('load', () => {

    function addBtn() {
        let changeThemeBtn = document.createElement('div').innerHTML =
            '<div class="theme-btn" ' +
            'style="width: 100px;' +
            'height: 40px;' +
            'background-color: #35444F;' +
            'margin-top: 50px;' +
            'text-align: center;' +
            'padding: 5px;' +
            'user-select: none;' +
            'color: #ffffff;">' +
            'Change theme!</div>';

        document.querySelector('.nav-left').insertAdjacentHTML('afterend', changeThemeBtn);
    }
    addBtn();

    let currTheme = localStorage.getItem('theme');

    const defaultTheme = {
        topNavBg : '#35444F',
        topNavBgHov : '#222F3A',
        botNavBg : '#63696e7a',
        headColor : '#4BCAFF'
    }

    const customTheme = {
        topNavBg : '#E14D2A',
        topNavBgHov : '#820000',
        botNavBg : '#FD841F',
        headColor : '#FF9494'
    }

    currTheme === 'defaultTheme' ? applyThemeStyles(defaultTheme) : applyThemeStyles(customTheme);

    document.querySelector('.theme-btn').addEventListener('click', changeTheme);

    function changeTheme() {
        let currTheme = localStorage.getItem('theme');
        if (currTheme === 'defaultTheme') {
            localStorage.setItem('theme', 'customTheme');
            applyThemeStyles(customTheme);
        } else {
            localStorage.setItem('theme', 'defaultTheme');
            applyThemeStyles(defaultTheme);
        }
    }

    function applyThemeStyles(themeName) {
        document.querySelector('.logo-top:nth-child(1)').style.color = themeName.headColor;
        document.querySelector('.nav-top').style.backgroundColor = themeName.topNavBg;
        document.querySelectorAll('.nav-top > a').forEach(el => {
            el.style.backgroundColor = themeName.topNavBg;
            el.onmouseover = function() { this.style.backgroundColor = themeName.topNavBgHov};
            el.onmouseout = function() { this.style.backgroundColor = themeName.topNavBg};
        });
        document.querySelector('.bottom').style.backgroundColor = themeName.botNavBg;
        document.querySelector('.theme-btn').style.backgroundColor = themeName.topNavBg;
    }
})






















